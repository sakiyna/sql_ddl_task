CREATE DATABASE healthcare_db;
USE healthcare_db;

CREATE SCHEMA healthcare_schema;

-- Create tables:
CREATE TABLE Patients (
    patient_id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    gender VARCHAR(10) NOT NULL CHECK (gender IN ('Male', 'Female')),
    dob DATE NOT NULL,
    email VARCHAR(100),
    phone VARCHAR(15),
    address VARCHAR(255),
    record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE Doctors (
    doctor_id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    specialization VARCHAR(100),
    record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE Appointments (
    appointment_id SERIAL PRIMARY KEY,
    patient_id INT NOT NULL,
    doctor_id INT NOT NULL,
    appointment_date TIMESTAMP NOT NULL,
    description VARCHAR(255),
    FOREIGN KEY (patient_id) REFERENCES Patients(patient_id),
    FOREIGN KEY (doctor_id) REFERENCES Doctors(doctor_id),
    record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE Medications (
    medication_id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    manufacturer VARCHAR(100),
    record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE Prescriptions (
    prescription_id SERIAL PRIMARY KEY,
    patient_id INT NOT NULL,
    doctor_id INT NOT NULL,
    medication_id INT NOT NULL,
    dosage VARCHAR(50),
    frequency VARCHAR(50),
    start_date DATE NOT NULL,
    end_date DATE,
    FOREIGN KEY (patient_id) REFERENCES Patients(patient_id),
    FOREIGN KEY (doctor_id) REFERENCES Doctors(doctor_id),
    FOREIGN KEY (medication_id) REFERENCES Medications(medication_id),
    record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Check constraints:
ALTER TABLE Appointments ADD CONSTRAINT chk_appointment_date CHECK (appointment_date > '2000-01-01');

ALTER TABLE Prescriptions ADD CONSTRAINT chk_dosage_non_negative CHECK (dosage::INTEGER >= 0);

ALTER TABLE Patients ADD CONSTRAINT unique_email UNIQUE (email);

-- Data insertion for Patients table:
INSERT INTO Patients (name, gender, dob, email, phone, address)
VALUES
    ('Jim Thompson', 'Male', '1990-05-15', 'jim@example.com', '123-456-7890', '123 Main St'),
    ('Will Smith', 'Male', '1989-08-20', 'will@example.com', '987-654-3210', '456 Elm St');





